from .views import TableViewSet
from django.urls import path, include
from rest_framework.routers import DefaultRouter


router = DefaultRouter()
router.register(r'', TableViewSet, basename='Tables')

urlpatterns = [
    path('', include(router.urls)),
]
