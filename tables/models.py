from django.db import models
from restaurants.models import Venue


class Table(models.Model):
    name = models.CharField(max_length=50)
    top = models.IntegerField()
    left = models.IntegerField()
    venue = models.ForeignKey(Venue, on_delete=models.CASCADE, related_name="tables")
