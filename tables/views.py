from rest_framework import viewsets
from .serializers import TableSerializer
from .models import Table


class TableViewSet(viewsets.ModelViewSet):
    serializer_class = TableSerializer
    queryset = Table.objects.all()
