from rest_framework import serializers
from tables.serializers import TableSerializer
from .models import Restaurant, Venue


class VenueSerializer(serializers.ModelSerializer):
    tables = TableSerializer(many=True, read_only=True)

    class Meta:
        model = Venue
        fields = '__all__'


class RestaurantDetailSerializer(serializers.ModelSerializer):
    venues = VenueSerializer(many=True, read_only=True)

    plan_display = serializers.CharField(
        source='get_plan_display', read_only=True
    )

    class Meta:
        model = Restaurant
        fields = '__all__'


class RestaurantSerializer(serializers.ModelSerializer):

    plan_display = serializers.CharField(
        source='get_plan_display', read_only=True
    )

    class Meta:
        model = Restaurant
        fields = '__all__'


class VenueDetailSerializer(serializers.ModelSerializer):
    tables = TableSerializer(many=True, read_only=True)
    restaurant = RestaurantSerializer(many=False, read_only=True)

    class Meta:
        model = Venue
        fields = '__all__'
