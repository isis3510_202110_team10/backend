from django.db import models
import uuid


class Restaurant(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50)
    logo = models.FileField(blank=True, null=True)

    # TODO
    #   colors

    FREE = 0
    TRIAL = 1
    BASIC = 2
    BUSINESS = 3
    PREMIUM = 4
    CHAIN = 5

    PLAN_CHOICES = [
        (FREE, 'Gratis'),
        (TRIAL, 'Prueba'),
        (BASIC, 'Basico'),
        (BUSINESS, 'Negocio'),
        (PREMIUM, 'Premium'),
        (CHAIN, 'Cadena'),
    ]

    plan = models.IntegerField(
        choices=PLAN_CHOICES,
        default=TRIAL,
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Venue(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=50)

    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)

    restaurant = models.ForeignKey(
        Restaurant,
        on_delete=models.CASCADE,
        blank=True,
        related_name='venues'
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.name} ({self.restaurant.name})'
