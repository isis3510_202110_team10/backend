from django.contrib import admin

from menu.models import Category
from .models import Restaurant, Venue
from ordered_model.admin import OrderedTabularInline, OrderedInlineModelAdminMixin


class CategoryAdminInline(OrderedTabularInline):
    model = Category
    fields = ('restaurant', 'name', 'order', 'move_up_down_links',)
    readonly_fields = ('name', 'order', 'move_up_down_links',)
    extra = 1
    ordering = ('order',)


@admin.register(Restaurant)
class RestaurantAdmin(OrderedInlineModelAdminMixin, admin.ModelAdmin):
    inlines = (CategoryAdminInline,)


@admin.register(Venue)
class VenueAdmin(admin.ModelAdmin):
    pass
