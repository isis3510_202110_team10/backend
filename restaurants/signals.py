from django.conf import settings
from django.db.models.signals import post_save
from profiles.models import Profile
from .models import Restaurant


def save_restaurant_to_profile(sender, instance, created, **kwargs):
    import inspect
    for frame_record in inspect.stack():
        if frame_record[3] == 'get_response':
            request = frame_record[0].f_locals['request']

            user = request.user

            if not user.is_staff and not user.is_superuser:
                profile = request.user.profile

                if profile.restaurant is None:
                    profile.restaurant = instance
                    profile.save()


post_save.connect(save_restaurant_to_profile, sender=Restaurant)
