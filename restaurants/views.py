from .models import Restaurant, Venue
from .serializers import RestaurantDetailSerializer, VenueSerializer
from rest_framework import viewsets


class RestaurantViewSet(viewsets.ModelViewSet):
    serializer_class = RestaurantDetailSerializer

    def get_queryset(self):
        restaurant_id = self.request.user.profile.restaurant_id
        return Restaurant.objects.filter(id=restaurant_id)


class VenueViewSet(viewsets.ModelViewSet):
    serializer_class = VenueSerializer

    def get_queryset(self):
        restaurant = self.request.user.profile.restaurant
        print(restaurant)
        return Venue.objects.filter(restaurant=restaurant)

    def perform_create(self, serializer):
        restaurant = self.request.user.profile.restaurant
        serializer.save(restaurant=restaurant)
