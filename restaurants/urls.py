from .views import RestaurantViewSet, VenueViewSet
from django.urls import path, include
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'venues', VenueViewSet, basename='Venues')
router.register(r'', RestaurantViewSet, basename='Restaurants')

urlpatterns = [
    path('', include(router.urls)),
]
