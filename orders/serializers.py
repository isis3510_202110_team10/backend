from rest_framework import serializers

from tables.serializers import TableSerializer
from .models import Order, SelectedChoice, OrderItem


class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = '__all__'


class SelectedChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = SelectedChoice
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):
    choices = SelectedChoiceSerializer(many=True, read_only=True)
    table = TableSerializer(many=False, read_only=True)

    items = OrderItemSerializer(many=True, read_only=False)

    class Meta:
        model = Order
        fields = '__all__'
