from django.contrib import admin

# Register your models here.
from .models import Order, OrderItem, TableOrder, ClientNameOrder, DeliveryOrder, SelectedChoice


@admin.register(TableOrder)
class TableOrderAdmin(admin.ModelAdmin):
    pass


@admin.register(ClientNameOrder)
class ClientNameOrderAdmin(admin.ModelAdmin):
    pass


@admin.register(DeliveryOrder)
class DeliveryOrderAdmin(admin.ModelAdmin):
    pass


@admin.register(SelectedChoice)
class SelectedChoiceAdmin(admin.ModelAdmin):
    pass
