from django.db import models
from restaurants.models import Venue
from tables.models import Table
from django.core.validators import MaxValueValidator, MinValueValidator
from polymorphic.models import PolymorphicModel
from phonenumber_field.modelfields import PhoneNumberField
import uuid


class Order(PolymorphicModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    venue = models.ForeignKey(Venue, on_delete=models.CASCADE)

    CASH = 0
    CARD = 1
    DIGITAL = 2

    PAYMENT_METHOD_CHOICES = [
        (CASH, 'Gratis'),
        (CARD, 'Datáfono'),
        (DIGITAL, 'Digital'),
    ]

    payment_method = models.IntegerField(
        choices=PAYMENT_METHOD_CHOICES,
        blank=True,
        null=True
    )

    created_at = models.DateTimeField(auto_now_add=True)


class TableOrder(Order):
    table = models.ForeignKey(Table, on_delete=models.SET_NULL, null=True, blank=False)


class ClientNameOrder(Order):
    client_name = models.CharField(max_length=50)


class DeliveryOrder(Order):
    client_name = models.CharField(max_length=50)
    address = models.CharField(max_length=100)
    phone_number = PhoneNumberField()
    comments = models.TextField(max_length=50)


class OrderItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)

    product_name = models.CharField(max_length=50)
    quantity = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(100)])
    comments = models.TextField(max_length=200)

    price = models.CharField(max_length=50)
    image_url = models.TextField(null=True)
    # category = models.CharField(max_length=50)
    price = models.CharField(max_length=200, default=0)

    CREATED = 0
    USER_CANCELLED = 1
    NOT_ACCEPTED = 2
    ACCEPTED = 3
    CANCELED_DURING_PREPARATION = 4
    DELIVERED = 5
    PAYED = 6
    RATED = 7

    STATUS_CHOICES = [
        (CREATED, 'Creada'),
        (USER_CANCELLED, 'Cancelada por usuario'),
        (NOT_ACCEPTED, 'No aceptada'),
        (ACCEPTED, 'Aceptada'),
        (CANCELED_DURING_PREPARATION, 'Cancelada durante la preparación'),
        (DELIVERED, 'Entregada'),
        (PAYED, 'Pagada'),
        (RATED, 'Calificada'),
    ]

    status = models.IntegerField(
        choices=STATUS_CHOICES,
        default=CREATED,
    )


class SelectedChoice(models.Model):
    name = models.CharField(max_length=50)
    option_name = models.CharField(max_length=50)

    order_item = models.ForeignKey(OrderItem, on_delete=models.CASCADE, related_name="choices")
    extra_cost = models.FloatField(validators=[MinValueValidator(0.0)])
