from rest_framework import (
    viewsets, status,
)
from rest_framework.response import Response

from restaurants.models import Venue
from .models import (
    Category, Item, CategoryItemThroughModel, ItemOption
)
from .serializers import (
    CategorySerializer, CompleteMenuSerializer, ItemSerializer, MoveCategorySerializer, ItemOptionSerializer,
    NotAvailableVenueSerializer
)

from rest_framework import generics
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView


class OptionsViewSet(viewsets.ModelViewSet):
    serializer_class = ItemOptionSerializer
    queryset = ItemOption.objects.all()


class CategoriesViewSet(viewsets.ModelViewSet):
    serializer_class = CategorySerializer

    def get_queryset(self):
        restaurant = self.request.user.profile.restaurant
        return Category.objects.filter(restaurant=restaurant)

    def perform_create(self, serializer):
        restaurant = self.request.user.profile.restaurant
        serializer.save(restaurant=restaurant)


class ItemsViewSet(viewsets.ModelViewSet):
    serializer_class = ItemSerializer

    def get_queryset(self):
        restaurant = self.request.user.profile.restaurant
        return Item.objects.filter(restaurant=restaurant)

    def create(self, request):
        serializer = ItemSerializer(data=request.data)
        restaurant = request.user.profile.restaurant
        if serializer.is_valid():
            item = serializer.save(restaurant=restaurant)
            id = item.id
            # data.update({'id-url': product.url})  # attaching key-value to the dictionary
            data = serializer.data
            data.update({'id': id})
            print(data)
            return Response(data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    """
    def perform_create(self, serializer):
        restaurant = self.request.user.profile.restaurant
        categories = self.request.data.get('categories')
        # categories = self.request.data.get('categories')

        # for category in categories:
        #    CategoryItemThroughModel.objects.create(category=category, item = )
        item = serializer.save(restaurant=restaurant)
        id = item.id
        print(id)

        data = serializer.data
        print(data.get('id'))
        data.update({'id': id})
        print(data)
    """


class CompleteMenuList(generics.ListAPIView):
    serializer_class = CompleteMenuSerializer

    def get_queryset(self):
        restaurant = self.request.user.profile.restaurant

        return Category.objects.filter(
            restaurant=restaurant
        )


class DinerGetMenu(generics.ListAPIView):
    serializer_class = CompleteMenuSerializer
    permission_classes = [AllowAny]

    def get_queryset(self):
        venue = Venue.objects.get(id=self.request.query_params.get('venue'))
        restaurant = venue.restaurant

        menu = Category.objects.filter(
            restaurant=restaurant
        ).exclude(not_available_at__in=[venue])

        results = list()

        results.append({'menu': menu, 'venue': venue})

        return results


class MoveCategory(APIView):

    def post(self, request, format=None):
        serializer = MoveCategorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class NotAvailableVenue(APIView):

    def post(self, request, format=None):
        serializer = NotAvailableVenueSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
