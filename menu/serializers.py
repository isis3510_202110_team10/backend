from abc import ABC

from rest_framework import serializers

from restaurants.models import Venue
from restaurants.serializers import VenueDetailSerializer
from .models import Category, Item, ItemOption, OptionChoice


class OptionChoiceSerializer(serializers.ModelSerializer):
    not_available_at = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    def create(self, validated_data):
        choices = validated_data.pop('choices')
        validated_data.pop('not_available_at')

        option = ItemOption.objects.create(**validated_data)

        for choice in choices:
            option.choices.add(OptionChoice.objects.create(**choice))

        return option

    class Meta:
        model = OptionChoice
        fields = '__all__'


class ItemOptionSerializer(serializers.ModelSerializer):
    choices = OptionChoiceSerializer(many=True)
    not_available_at = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    def create(self, validated_data):
        choices = validated_data.pop('choices')

        option = ItemOption.objects.create(**validated_data)

        option.choices.set(choices)

        return option

    class Meta:
        model = ItemOption
        fields = (
            'item',
            'id',
            'name',
            'description',
            'not_available_at',
            'min',
            'max',
            'choices'
        )


class ItemSerializer(serializers.ModelSerializer):
    options = ItemOptionSerializer(many=True, read_only=True)
    categories = serializers.PrimaryKeyRelatedField(many=True, queryset=Category.objects.all())
    not_available_at = serializers.PrimaryKeyRelatedField(many=True, queryset=Venue.objects.all())

    def create(self, validated_data):
        categories = validated_data.pop('categories')
        validated_data.pop('not_available_at')

        item = Item.objects.create(**validated_data)

        # categories = self.data.get('categories')
        print(categories)

        for category in categories:
            item.categories.add(category)

        print(item.id)

        return item

    def update(self, instance, validated_data):
        not_available_at_list = validated_data.pop('not_available_at')
        instance = super(ItemSerializer, self).update(instance, validated_data)

        print(not_available_at_list)
        instance.not_available_at.clear()

        for not_available_at_data in not_available_at_list:
            print(not_available_at_data)
            instance.not_available_at.add(not_available_at_data)

        instance.save()

        return instance

    class Meta:
        model = Item
        fields = (
            'id',
            'name',
            'description',
            'restaurant',
            'not_available_at',
            'price',
            'preparation_time',
            'image',
            'is_featured',
            'is_recommended',
            'is_spicy',
            'has_nuts',
            'is_vegan',
            'is_vegetarian',
            'is_gluten_free',
            'options',
            'categories'
        )


class CategorySerializer(serializers.ModelSerializer):
    items = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Category
        fields = '__all__'


class ReadOnlyMenuSerializer(serializers.ModelSerializer):
    items = ItemSerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = '__all__'


class CompleteMenuSerializer(serializers.Serializer):
    menu = ReadOnlyMenuSerializer(many=True, read_only=True)
    venue = VenueDetailSerializer(many=False, read_only=True)

    def create(self, validated_data):
        return None

    def update(self, instance, validated_data):
        return None


UP = 'up'
DOWN = 'down'


class MoveCategorySerializer(serializers.Serializer):
    id = serializers.IntegerField()

    move = serializers.ChoiceField(
        choices=[UP, DOWN],
    )

    def save(self):
        category_id = self.validated_data['id']
        move = self.validated_data['move']

        category = Category.objects.get(id=category_id)

        if move == UP:
            category.up()
        elif move == DOWN:
            category.down()


ADD = 'add'
REMOVE = 'remove'


class NotAvailableVenueSerializer(serializers.Serializer):
    item_id = serializers.IntegerField()
    venue_id = serializers.CharField()

    operation = serializers.ChoiceField(
        choices=[ADD, REMOVE],
    )

    def save(self):
        item_id = self.validated_data['item_id']
        venue_id = self.validated_data['venue_id']
        operation = self.validated_data['operation']

        item = Item.objects.get(id=item_id)

        if operation == ADD:
            item.not_available_at.add(venue_id)
        else:
            item.not_available_at.remove(venue_id)
