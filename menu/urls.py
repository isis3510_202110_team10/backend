from .views import CategoriesViewSet, ItemsViewSet, CompleteMenuList, DinerGetMenu, MoveCategory, OptionsViewSet, NotAvailableVenue
from django.urls import path, include
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'categories', CategoriesViewSet, basename='Categories')
router.register(r'items', ItemsViewSet, basename='Items')
router.register(r'options', OptionsViewSet, basename='Options')

urlpatterns = [
    path('', CompleteMenuList.as_view()),
    path('diner', DinerGetMenu.as_view()),
    path('move_category', MoveCategory.as_view()),
    path('not_available_venue', NotAvailableVenue.as_view()),
    path('', include(router.urls)),
]
