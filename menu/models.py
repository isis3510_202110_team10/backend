from django.db import models
from django.core.validators import MinValueValidator
from restaurants.models import Restaurant, Venue
from sortedm2m.fields import SortedManyToManyField
from ordered_model.models import OrderedModel


class VenueNotAvailableMap(models.Model):
    not_available_at = models.ManyToManyField(Venue, blank=True)

    class Meta:
        abstract = True


class Item(VenueNotAvailableMap):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    price = models.FloatField(validators=[MinValueValidator(0.0)])
    preparation_time = models.IntegerField(validators=[MinValueValidator(0.0)], blank=True, null=True)
    image = models.FileField(blank=True, null=True)

    restaurant = models.ForeignKey(
        Restaurant,
        on_delete=models.CASCADE,
        related_name='items',
        blank=True
    )

    is_spicy = models.BooleanField(blank=True, null=False, default=False)
    has_nuts = models.BooleanField(blank=True, null=False, default=False)
    is_vegan = models.BooleanField(blank=True, null=False, default=False)
    is_vegetarian = models.BooleanField(blank=True, null=False, default=False)
    is_gluten_free = models.BooleanField(blank=True, null=False, default=False)
    is_featured = models.BooleanField(blank=True, null=False, default=False)
    is_recommended = models.BooleanField(blank=True, null=False, default=False)

    def __str__(self):
        return f'{self.name} ({self.restaurant})'


class Category(VenueNotAvailableMap, OrderedModel):
    name = models.CharField(max_length=50)
    items = models.ManyToManyField(Item, related_name="categories", blank=True)

    restaurant = models.ForeignKey(
        Restaurant,
        on_delete=models.CASCADE,
        related_name='categories',
        blank=True
    )

    order_with_respect_to = 'restaurant'

    def __str__(self):
        return f'{self.name} ({self.restaurant.name})'

    class Meta:
        verbose_name_plural = "categories"
        ordering = ['order']


class CategoryItemThroughModel(OrderedModel):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    order_with_respect_to = 'category'

    class Meta:
        ordering = ('category', 'item')


class ItemOption(VenueNotAvailableMap):
    item = models.ForeignKey(Item, on_delete=models.CASCADE, related_name="options")
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True, null=True)
    min = models.IntegerField(validators=[MinValueValidator(0)], default=0)
    max = models.IntegerField(validators=[MinValueValidator(1)], default=1)

    def __str__(self):
        return f'{self.name} ({self.item})'


class OptionChoice(VenueNotAvailableMap):
    option = models.ForeignKey(ItemOption, on_delete=models.CASCADE, related_name="choices")
    name = models.CharField(max_length=50)
    extra_cost = models.FloatField(validators=[MinValueValidator(0.0)])

    def __str__(self):
        return f'{self.name} ({self.option})'
