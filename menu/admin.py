from django.contrib import admin

from restaurants.models import Venue
from .models import Category, Item, ItemOption, OptionChoice, CategoryItemThroughModel
from ordered_model.admin import OrderedTabularInline, OrderedInlineModelAdminMixin, OrderedModelAdmin


class CategoryItemThroughModelTabularInline(OrderedTabularInline):
    model = CategoryItemThroughModel
    fields = ('item', 'order', 'move_up_down_links',)
    readonly_fields = ('order', 'move_up_down_links',)
    extra = 1
    ordering = ('order',)


@admin.register(Category)
class CategoryAdmin(OrderedInlineModelAdminMixin, OrderedModelAdmin):
    # inlines = (CategoryItemThroughModelTabularInline,)

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if obj:
            form.base_fields['not_available_at'].queryset = Venue.objects.filter(
                restaurant=obj.restaurant
            )
        return form


class OptionAdminInline(admin.TabularInline):
    model = ItemOption


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    inlines = (OptionAdminInline,)

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if obj:
            form.base_fields['not_available_at'].queryset = Venue.objects.filter(
                restaurant=obj.restaurant
            )
        return form


class OptionChoiceAdminInline(admin.TabularInline):
    model = OptionChoice


@admin.register(ItemOption)
class ItemOptionAdmin(admin.ModelAdmin):
    inlines = (OptionChoiceAdminInline,)

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if obj:
            form.base_fields['not_available_at'].queryset = Venue.objects.filter(
                restaurant=obj.item.restaurant
            )
        return form


@admin.register(OptionChoice)
class OptionChoiceAdmin(admin.ModelAdmin):
    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if obj:
            form.base_fields['not_available_at'].queryset = Venue.objects.filter(
                restaurant=obj.option.item.restaurant
            )
        return form
