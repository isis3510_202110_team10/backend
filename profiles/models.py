from django.db import models
from restaurants.models import Restaurant
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    restaurant = models.ForeignKey(Restaurant, related_name='profiles', null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.user.username} Profile'
