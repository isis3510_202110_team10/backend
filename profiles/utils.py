import json
import os

from django.contrib.auth import authenticate
import jwt
import requests


def jwt_get_username_from_payload_handler(payload):
    username = payload.get('sub').replace('|', '.')
    email = payload.get('email')
    first_name = payload.get('given_name')
    last_name = payload.get('family_name')

    user = authenticate(remote_user=username)
    user.email = email
    user.first_name = '' if first_name is None else str(first_name)
    user.last_name = '' if last_name is None else str(last_name)
    user.set_unusable_password()
    user.full_clean()

    from django.utils import timezone
    user.last_login = timezone.now()

    user.save()
    return username


def jwt_decode_token(token):
    header = jwt.get_unverified_header(token)
    auth0_domain = os.environ.get('AUTH0_DOMAIN')
    jwks = requests.get('https://{}/.well-known/jwks.json'.format(auth0_domain)).json()
    public_key = None
    for jwk in jwks['keys']:
        if jwk['kid'] == header['kid']:
            public_key = jwt.algorithms.RSAAlgorithm.from_jwk(json.dumps(jwk))

    if public_key is None:
        raise Exception('Public key not found.')

    issuer = 'https://{}/'.format(auth0_domain)

    try:
        api_identifier = os.environ.get('API_IDENTIFIER_NATIVE')
        decoded = jwt.decode(token, public_key, audience=api_identifier, issuer=issuer, algorithms=['RS256'])
    except jwt.exceptions.InvalidAudienceError:
        api_identifier = os.environ.get('API_IDENTIFIER_WEB')
        decoded = jwt.decode(token, public_key, audience=api_identifier, issuer=issuer, algorithms=['RS256'])

    return decoded
