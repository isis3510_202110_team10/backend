from .settings import *

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': 'ZwhuHnQe7JkBUA3G3Vat',
        'HOST': 'compelat-develop.cohfxp7xeooc.us-east-2.rds.amazonaws.com',
        'PORT': '5432',
    }
}
